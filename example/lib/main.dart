import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quality_wallpaper/quality_wallpaper.dart';

void main() =>
    runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

enum Screen {
  Home,
  Lock,
  Both,
  // \todo System,
}

enum Source {
  Undefined,
  Assets,
  Files,
  Internet,
}

class _MyAppState extends State<MyApp> {
  static const List<String> _images = [
    'assets/image/a.jpg',
    'assets/image/b.jpg',
    'https://images.pexels.com/photos/2772854/pexels-photo-2772854.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/1368388/pexels-photo-1368388.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/2417842/pexels-photo-2417842.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/2406776/pexels-photo-2406776.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  ];

  Source sourceFrom(int i) {
    if (i == -1) {
      return Source.Files;
    }
    final s = _images[i];
    if (s.startsWith('asset')) {
      return Source.Assets;
    }
    if (s.startsWith('http')) {
      return Source.Internet;
    }
    return Source.Undefined;
  }

  bool _processing = false;
  double _result = 0;

  Future<String> _imagePath;
  String _preparedImagePath;

  TextStyle get textStyle => TextStyle(fontSize: 24);

  @override
  void initState() {
    super.initState();

    _imagePath = _createImageFromAsset();
  }

  Future<String> _createImageFromAsset() async {
    // copy image from assets
    //final dir = await getApplicationDocumentsDirectory();
    final dir = await QualityWallpaper.deviceLocalPath;
    final path = dir + '/temp_file_auto.jpg';
    print('path for save $path');
    // copy file from Assets folder to Path folder only if not already there
    //if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
    final ByteData data = await rootBundle.load('assets/image/b.jpg');
    final buf = data.buffer;
    File(path)
      ..writeAsBytesSync(
          buf.asUint8List(data.offsetInBytes, data.lengthInBytes));
    //}
    return path;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: [
                _buildInfoItem(),
                // \todo _buildAssetItem(0, Screen.Home),
                _buildFileItem(Screen.Home),
                _buildDownloadItem(2, Screen.Home),
                _buildDownloadItem(3, Screen.Lock),
                _buildDownloadItem(4, Screen.Both),
                // \todo _buildDownloadItem(3, Screen.System),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildInfoItem() {
    final platformVersion = FutureBuilder(
      future: QualityWallpaper.platformVersion,
      builder: (context, snapshot) {
        return _futureSnapshotString('platform version', snapshot);
      },
    );
    final deviceLocalPath = FutureBuilder(
      future: QualityWallpaper.deviceLocalPath,
      builder: (context, snapshot) {
        return _futureSnapshotString('device local path', snapshot);
      },
    );
    return Container(
      padding: const EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          platformVersion,
          SizedBox(height: 12),
          deviceLocalPath,
        ],
      ),
    );
  }

  Widget _futureSnapshotString(String label, AsyncSnapshot<String> snapshot) {
    if (snapshot.hasError) {
      return Text(snapshot.error.toString(), style: textStyle);
    }
    if (!snapshot.hasData) {
      return Text('No Data', style: textStyle);
    }
    return Text('$label\n${snapshot.data}', style: textStyle);
  }

  Widget _buildDownloadItem(int i, Screen screen) {
    return Stack(
      children: [
        Image.network(
          _images[i],
          fit: BoxFit.fitWidth,
        ),
        _buildInstallButton(i, screen),
        _buildDialog(),
      ],
    );
  }

  Widget _buildAssetItem(int i, Screen screen) {
    return Stack(
      children: [
        Image.asset(
          _images[i],
          fit: BoxFit.fitWidth,
        ),
        _buildInstallButton(i, screen),
        _buildDialog(),
      ],
    );
  }

  /// Save file from assets to gallery and set it
  Widget _buildFileItem(Screen screen) {
    return FutureBuilder(
      future: _imagePath,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text(snapshot.error.toString(), style: textStyle);
        }
        if (!snapshot.hasData) {
          return Text('No Data', style: textStyle);
        }
        _preparedImagePath = snapshot.data;
        print('path to file $_preparedImagePath');
        return Stack(
          children: [
            Image.file(File(_preparedImagePath)),
            _buildInstallButton(-1, screen),
            _buildDialog(),
          ],
        );
      },
    );
  }

  Widget _buildInstallButton(int i, Screen screen) {
    return Positioned(
      left: 12,
      bottom: 12,
      child: RaisedButton(
        onPressed: () => _onPressInstall(i, screen),
        textColor: Colors.white,
        padding: EdgeInsets.zero,
        child: Center(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xFF0D47A1),
                  Color(0xFF1976D2),
                  Color(0xFF42A5F5),
                ],
              ),
            ),
            padding: const EdgeInsets.all(12),
            child: Text(
              'Set to $screen from ${sourceFrom(i)}',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDialog() {
    final int result = _result.round();
    return Center(
      child: _processing
          ? Container(
              height: 120,
              width: 240,
              child: Card(
                color: Colors.black,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(height: 24),
                    Text(
                      'Downloading $result%',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ),
            )
          : Container(),
    );
  }

  _onPressInstall(int i, Screen screen) {
    if (sourceFrom(i) == Source.Assets) {
      _onPressInstallFromAsset(screen);
      return;
    }

    if (sourceFrom(i) == Source.Files) {
      _onPressInstallFromFile(screen);
      return;
    }

    if (sourceFrom(i) == Source.Internet) {
      _onPressInstallFromInternet(i, screen);
      return;
    }

    throw 'Undefined prefix.';
  }

  _onPressInstallFromAsset(Screen screen) {
    QualityWallpaper.initWithAsset('assets/image/a.jpg').then((_) {
      _setWallpaper(screen);
      _processing = false;
      setState(() {});
    });

    _processing = true;
    setState(() {});
  }

  _onPressInstallFromFile(Screen screen) {
    print('Start $_preparedImagePath to $screen');
    QualityWallpaper.initWithFile(_preparedImagePath);
    _setWallpaper(screen);
    _processing = false;
    setState(() {});
  }

  _onPressInstallFromInternet(int i, Screen screen) {
    print('Start ${_images[i]} to $screen');
    QualityWallpaper.initWithUrl(_images[i])
      ..listen((double data) {
        print('Data received $data');
        _result = data;
        setState(() {});
      }, onDone: () async {
        await _setWallpaper(screen);
        print('Done');
        _processing = false;
        setState(() {});
      }, onError: (error) {
        print('Error $error');
        _processing = false;
        setState(() {});
      });

    _processing = true;
    setState(() {});
  }

  _setWallpaper(Screen screen) async {
    print('Start with $screen');
    switch (screen) {
      case Screen.Home:
        await QualityWallpaper.homeScreen()
            .then((s) => print('QualityWallpaper.homeScreen() Completed. $s'));
        break;

      case Screen.Lock:
        await QualityWallpaper.lockScreen()
            .then((s) => print('QualityWallpaper.lockScreen() Completed. $s'));
        break;

      case Screen.Both:
        await QualityWallpaper.bothScreen()
            .then((s) => print('QualityWallpaper.bothScreen() Completed. $s'));

        break;
      /* \todo
      case Screen.System:
        await QualityWallpaper.systemScreen();
        break;
      */

      default:
        print('Not recognized a screen $screen');
    }
  }
}
