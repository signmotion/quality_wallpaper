package fun.signmotion.quality_wallpaper;

import android.app.WallpaperManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;

import io.flutter.Log;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

public class QualityWallpaperPlugin extends FlutterActivity implements MethodCallHandler {
    static final String CHANNEL = "quality_wallpaper";

    /* \todo
    public static final String ERROR_FILE_NOT_FOUND =
            "Error: File not found.";
    public static final String ERROR_BITMAP_NOT_RECOGNIZED =
            "Error: Bitmap not recognized.";
    */

    FlutterActivity activity;
    private final Registrar registrar;
    private final MethodChannel channel;

    public String result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.channel.setMethodCallHandler(this);
    }

    private Context getActiveContext() {
        return (registrar.activity() != null) ? registrar.activity() : registrar.context();
    }

    /**
     * HomeScreen
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), CHANNEL);
        channel.setMethodCallHandler(new QualityWallpaperPlugin(
                (FlutterActivity) registrar.activity(), channel, registrar));
    }

    private QualityWallpaperPlugin(FlutterActivity activity, MethodChannel channel, Registrar registrar) {
        this.activity = activity;
        this.channel = channel;
        this.registrar = registrar;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result callResult) {
        switch (call.method) {
            case "getPlatformVersion":
                callResult.success("" + Build.VERSION.RELEASE);
                break;
            case "getDeviceLocalPath":
                File dir = getDeviceLocalPath();
                callResult.success((dir == null) ? "?" : dir.getPath());
                break;
            case "homeScreen":
                callResult.success(setWallpaper(1, (String) call.arguments));
                break;
            case "lockScreen":
                callResult.success(setWallpaper(2, (String) call.arguments));
                break;
            case "bothScreen":
                callResult.success(setWallpaper(3, (String) call.arguments));
                break;
            case "systemScreen":
                callResult.success(setWallpaper(4, (String) call.arguments));
                break;
            default:
                callResult.notImplemented();
                break;
        }
    }

    private File getDeviceLocalPath() {
        return activity.getFilesDir();
    }

    private String setWallpaper(int id, String path) {

        result = "";

        Bitmap bitmap = getBitmap(path);
        if (bitmap == null) {
            return result;
        }

        // set bitmap to wallpaper
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(activity);
        if (id == 1) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    wallpaperManager.setBitmap(
                            bitmap,
                            null,
                            true,
                            WallpaperManager.FLAG_SYSTEM);
                    result = "Home screen set successfully with file " + path + ".";
                } else {
                    result = "To set home screen requires API level 24 and above.";
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } else if (id == 2) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    wallpaperManager.setBitmap(
                            bitmap,
                            null,
                            true,
                            WallpaperManager.FLAG_LOCK);
                    result = "Lock screen set successfully with file " + path + ".";
                } else {
                    result = "To set Lock screen requires API level 24 and above.";
                }
            } catch (IOException e) {
                result = e.toString();
                e.printStackTrace();
            }

        } else if (id == 3) {
            try {
                wallpaperManager.setBitmap(bitmap);
                result = "Home and Lock screen set successfully with file " + path + ".";
            } catch (IOException e) {
                result = e.toString();
                e.printStackTrace();
            }

        } else if (id == 4) {
            result = "Not implemented.";
            /* - \todo
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&
                        activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                    activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                } else {
                    //Uri uri = Uri.fromFile(file);
                    Uri contentURI = getImageContentUri(getActiveContext(), file);

                    Intent intent = new Intent(wallpaperManager.getCropAndSetWallpaperIntent(contentURI));
                    String mime = "image/*";
                    if (intent != null) {
                        intent.setDataAndType(contentURI, mime);
                    }
                    try {
                        registrar.activity().startActivityForResult(intent, 2);
                    } catch (ActivityNotFoundException e) {
                        //handle error
                        result = "Error to set wallpaper.";
                    }
                }
            }
            */
        }

        return result;
    }


    private Bitmap getBitmap(String path) {
        // from app folder
        //File dir = getDeviceLocalPath();
        File file = new File(path);
        if (!file.exists()) {
            result = "File by path '" + path + "' doesn't exists.";
            return null;
        }

        Bitmap r = BitmapFactory.decodeFile(file.getAbsolutePath());
        if (r == null) {
            result = "File by path '" + path + "' has undefined format.";
            return null;
        }

        return r;

        /* \todo from assets folder
        try {
            AssetFileDescriptor descriptor = getAssetFileDescriptor(path);
            return BitmapFactory.decodeFileDescriptor(descriptor.getFileDescriptor());
        } catch (Exception ex) {
            result = ex.toString();
            Log.e("Tag", path, ex);
        }
        */
    }


    public AssetFileDescriptor getAssetFileDescriptor(String path) throws Exception {
        AssetManager am = registrar.context().getAssets();
        String key = registrar.lookupKeyForAsset(path);
        return am.openFd(key);
    }


    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Log.d("Tag", filePath);
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "resultCode=" + resultCode + "requestCode=" + requestCode);
        if (resultCode == RESULT_OK) {
            result = "System screen set successfully.";
        } else if (resultCode == RESULT_CANCELED) {
            result = "Setting wallpaper cancelled.";
        } else {
            result = "Something went wrong.";
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private static String TAG = QualityWallpaperPlugin.class.getSimpleName();
}