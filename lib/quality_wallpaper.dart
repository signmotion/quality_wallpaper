import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class QualityWallpaper {
  static const channel = 'quality_wallpaper';
  static const downloadedFilename = '$channel.jpg';

  static Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static String savedFullPath;

  static const _platform = const MethodChannel(channel);

  static Future<String> get platformVersion async {
    return await _platform.invokeMethod<String>('getPlatformVersion');
  }

  static Future<String> get deviceLocalPath async {
    return await _platform.invokeMethod<String>('getDeviceLocalPath');
  }

  static Future<String> homeScreen() async {
    return await _platform.invokeMethod('homeScreen', savedFullPath);
  }

  static Future<String> lockScreen() async {
    return await _platform.invokeMethod('lockScreen', savedFullPath);
  }

  static Future<String> bothScreen() async {
    return await _platform.invokeMethod('bothScreen', savedFullPath);
  }

  /* \todo
  static Future<String> systemScreen() async {
    return await _platform.invokeMethod('systemScreen', savedFullPath);
  }
  */

  static Future initWithAsset(String file) async {
    final dir = await localPath;
    savedFullPath = '$dir/$file';
    debugPrint('savedFullPath $savedFullPath');
    throw 'Not implemented.';
    /*
    debugPrint('file $file');
    final last = file.lastIndexOf('/');
    String path = '';
    String basename = file;
    if (last != -1) {
      path = file.substring(0, last);
      basename = file.substring(last + 1);
    }
    debugPrint('path $path');
    debugPrint('basename $basename');
    final dir = await deviceLocalPath;
    savedFullPath = '$dir/$path/$basename';
    print('savedFullPath $savedFullPath');
    final directory = Directory('$dir/$path');
    if (!directory.existsSync()) {
      directory.createSync(recursive: true);
    }
    final ByteData data = await rootBundle.load(file);
    final buf = data.buffer;
    File(savedFullPath)
      ..writeAsBytesSync(
          buf.asUint8List(data.offsetInBytes, data.lengthInBytes));
    */
  }

  static initWithFile(String file) {
    debugPrint('file $file');
    savedFullPath = file;
  }

  // \todo optimize? Change to https://pub.dev/packages/dio_http_cache
  static Stream<double> initWithUrl(String url) async* {
    final StreamController<double> streamController = StreamController();
    //final dir = await getApplicationSupportDirectory();
    final dir = await deviceLocalPath;
    savedFullPath = '$dir/$downloadedFilename';
    debugPrint('savedFullPath $savedFullPath');
    try {
      Dio().download(
        url,
        savedFullPath,
        onReceiveProgress: (int received, int total) =>
            streamController.add(received * 100.0 / total),
      )
        ..then((Response response) => debugPrint('response $response'))
        ..catchError((ex) {
          debugPrint('$ex');
          streamController.add(0);
        })
        ..whenComplete(() {
          streamController.close();
        });
    } catch (ex) {
      debugPrint('$ex');
      throw ex;
    }

    yield* streamController.stream;
  }
}
